package com.vg.incubator.fft;

import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.vg.incubator.fft.user.UserRepo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FFTApplicationTests {

	@Inject
	private UserRepo userRepo;

	@Test
	public void contextLoads() {
		assertNotNull(userRepo);
	}

}
