package com.vg.incubator.fft.category;

public class CategoryNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 3212945046458213181L;

	public CategoryNotFoundException(String message) {
		super(message);
	}

}
