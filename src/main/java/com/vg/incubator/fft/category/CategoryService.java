package com.vg.incubator.fft.category;

import java.util.List;

public interface CategoryService {

	List<Category> getAllCategories();

	Category getCategory(Long id);

}
