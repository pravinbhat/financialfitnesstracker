package com.vg.incubator.fft.category;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categories")
public class CategoryController {

	private static final String ERROR_MESSAGE_HEADER = "errormessage";
	private CategoryService categoryService;

	@Inject
	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping
	public ResponseEntity<List<Category>> getCategorys() {
		return ResponseEntity.ok().body(categoryService.getAllCategories());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Category> getCategory(@PathVariable Long id) {
		try {
			return ResponseEntity.ok().body(categoryService.getCategory(id));
		} catch (CategoryNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		}
	}

	@PostMapping
	public ResponseEntity<Category> createCategory() {
		return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, "Category creation not allowed").build();
	}

	@CrossOrigin(origins = "*")
	@PutMapping("/{id}")
	public ResponseEntity<Category> updateCategory() {
		return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, "Category update not allowed").build();
	}

}