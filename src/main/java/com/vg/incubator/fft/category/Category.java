package com.vg.incubator.fft.category;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@SequenceGenerator(name = "Category_SEQ", sequenceName = "category_sequence")
public @Data @Entity class Category {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Category_SEQ")
	private Long id;

	@NotNull
	private String name, description;

	public Category(String category, String description, Integer recomendedBudgetPercent) {
		this.name = category;
		this.description = description;
	}

}
