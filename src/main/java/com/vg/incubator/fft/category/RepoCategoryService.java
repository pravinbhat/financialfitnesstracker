package com.vg.incubator.fft.category;

import static com.vg.incubator.fft.util.FFTUtility.CATEGORY_NOT_FOUND;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class RepoCategoryService implements CategoryService {

	private CategoryRepo categoryRepo;

	@Inject
	public RepoCategoryService(CategoryRepo categoryRepo) {
		this.categoryRepo = categoryRepo;
	}

	@Override
	public List<Category> getAllCategories() {
		List<Category> categorys = new ArrayList<>();
		categoryRepo.findAll().forEach(categorys::add);
		return categorys;
	}

	@Override
	public Category getCategory(Long id) {
		return categoryRepo.findById(id).orElseThrow(CATEGORY_NOT_FOUND);
	}

}
