package com.vg.incubator.fft.usercategory;

import static com.vg.incubator.fft.util.FFTUtility.getDateRangeOfNow;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vg.incubator.fft.category.CategoryNotFoundException;
import com.vg.incubator.fft.user.UserNotFoundException;
import com.vg.incubator.fft.usertransaction.Transaction;
import com.vg.incubator.fft.util.DateRange;

@RestController
@RequestMapping("/users/{userId}/categories")
public class UserCategoryController {

	private UserCategoryService userCategoryService;
	private static final String ERROR_MESSAGE_HEADER = "errormessage";

	@Inject
	public UserCategoryController(UserCategoryService userCategoryService) {
		this.userCategoryService = userCategoryService;
	}

	@GetMapping
	public ResponseEntity<List<Category>> getAllUserCategories(@PathVariable Long userId) {
		try {
			return ResponseEntity.ok().body(userCategoryService.getAllCategoriesWithTransactionInfoForUser(userId));
		} catch (UserNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		}
	}

	@GetMapping("/{categoryId}")
	public ResponseEntity<Category> getUserCategory(@PathVariable Long userId, @PathVariable Long categoryId) {
		try {
			return ResponseEntity.ok().body(userCategoryService.getUserCategoryWithTransactionInfo(userId, categoryId));
		} catch (UserNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		} catch (CategoryNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		}
	}

	@GetMapping("/{categoryId}/transactions")
	public ResponseEntity<List<Transaction>> getUserCategoryTransactions(@PathVariable Long userId,
			@PathVariable Long categoryId) {
		DateRange dateRange = getDateRangeOfNow();
		return ResponseEntity.ok().body(userCategoryService.getTransactionStreamBetweenDateRangeForCategory(userId,
				categoryId, dateRange.getBegginingOfCurrentMonth(), dateRange.getEndOfCurrentMonth()));
	}

	@PostMapping("/{categoryId}")
	public ResponseEntity<Category> createUserCategory() {
		return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, "Category creation not allowed").build();
	}

	@CrossOrigin(origins = "*")
	@PutMapping(value = "/{categoryId}")
	public ResponseEntity<Category> updateUserCategory(@PathVariable Long userId, @PathVariable Long categoryId,
			@RequestBody Target target) {
		if (null == target) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, "Target cannot be empty").build();
		}

		try {
			UserCategory uct = userCategoryService.updateUserCategory(userId, categoryId, target);
			return ResponseEntity.ok().body(userCategoryService
					.getUserCategoryWithTransactionInfo(uct.getUser().getId(), uct.getCategory().getId()));
		} catch (UserNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		} catch (CategoryNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		}
	}

}
