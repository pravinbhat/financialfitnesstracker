package com.vg.incubator.fft.usercategory;

import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.vg.incubator.fft.category.Category;
import com.vg.incubator.fft.user.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public @Data @Entity @AllArgsConstructor @NoArgsConstructor class UserCategory {
	@EmbeddedId
	private UserCategoryKey id;

	@ManyToOne
	@MapsId("user_id")
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne
	@MapsId("category_id")
	@JoinColumn(name = "category_id")
	private Category category;

	private BigDecimal goalAmount;
}
