package com.vg.incubator.fft.usercategory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.vg.incubator.fft.user.User;

public interface UserCategoryRepo extends CrudRepository<UserCategory, UserCategoryKey> {

	List<UserCategory> findByUser(User user);

}
