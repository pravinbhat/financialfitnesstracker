package com.vg.incubator.fft.usercategory;

import static com.vg.incubator.fft.util.FFTUtility.CATEGORY_NOT_FOUND;
import static com.vg.incubator.fft.util.FFTUtility.USER_NOT_FOUND;
import static com.vg.incubator.fft.util.FFTUtility.getDateRangeOfNow;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Named;

import com.vg.incubator.fft.category.CategoryRepo;
import com.vg.incubator.fft.user.User;
import com.vg.incubator.fft.user.UserRepo;
import com.vg.incubator.fft.usertransaction.TransactionRepo;
import com.vg.incubator.fft.util.DateRange;

@Named
public class RepoUserCategoryService implements UserCategoryService {

	private UserCategoryRepo userCategoryRepo;
	private UserRepo userRepo;
	private CategoryRepo categoryRepo;
	private TransactionRepo transactionRepo;

	@Inject
	public RepoUserCategoryService(UserCategoryRepo userCategoryRepo, UserRepo userRepo, CategoryRepo categoryRepo,
			TransactionRepo transactionRepo) {
		this.userCategoryRepo = userCategoryRepo;
		this.userRepo = userRepo;
		this.categoryRepo = categoryRepo;
		this.transactionRepo = transactionRepo;
	}

	@Override
	public List<Category> getAllCategoriesWithTransactionInfoForUser(Long userId) {
		Stream<UserCategory> userCategoryStream = getUserCategoryStream(findUserOrThrowNotFound(userId));
		// TODO refactor so one hit to the database gets all transactions, then filters
		// by category to avoid unnecessary database hits
		return userCategoryStream.map(userCategory -> calculateCurrentCategoryInfo(userCategory))
				.collect(Collectors.toList());
	}

	@Override
	public List<UserCategory> createCategoriesForUser(User user) {
		getCategoryStream().forEach(c -> createCategoryForUser(c, user));
		return userCategoryRepo.findByUser(user);
	}

	private void createCategoryForUser(com.vg.incubator.fft.category.Category c, User user) {
		UserCategoryKey userCategoryKey = new UserCategoryKey(user.getId(), c.getId());
		UserCategory userCategory = new UserCategory(userCategoryKey, user, c, ZERO);
		userCategoryRepo.save(userCategory);
	}

	private Stream<com.vg.incubator.fft.category.Category> getCategoryStream() {
		Spliterator<com.vg.incubator.fft.category.Category> allCategories = categoryRepo.findAll().spliterator();
		return StreamSupport.stream(allCategories, false);
	}

	@Override
	public Category getUserCategoryWithTransactionInfo(Long userId, Long categoryId) {
		findUserOrThrowNotFound(userId);
		findCategoryOrThrowNotFound(categoryId);
		UserCategoryKey userCategoryKey = new UserCategoryKey(userId, categoryId);

		return calculateCurrentCategoryInfo(userCategoryRepo.findById(userCategoryKey).get());
	}

	private Category calculateCurrentCategoryInfo(UserCategory userCategory) {
		DateRange dateRange = getDateRangeOfNow();
		BigDecimal currentAmountSpent = accumalteMonthsTransactionsByCategory(userCategory.getUser().getId(),
				dateRange.getBegginingOfCurrentMonth(), dateRange.getEndOfCurrentMonth(), userCategory);
		BigDecimal lastMonthAmountSpent = accumalteMonthsTransactionsByCategory(userCategory.getUser().getId(),
				dateRange.getBegginingOfLastMonth(), dateRange.getEndOfLastMonth(), userCategory);

		return new Category(userCategory.getCategory().getId(), userCategory.getCategory().getName(),
				userCategory.getGoalAmount(), currentAmountSpent, lastMonthAmountSpent);
	}

	private BigDecimal accumalteMonthsTransactionsByCategory(Long userId, LocalDate startDate, LocalDate endDate,
			UserCategory userCategory) {
		return getTransactionStreamBetweenDateRange(userId, startDate, endDate)
				.filter(t -> t.getCategoryId().equals(userCategory.getCategory().getId())).map(t -> t.getAmount())
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	@Override
	public UserCategory updateUserCategory(Long userId, Long categoryId, Target target) {
		findUserOrThrowNotFound(userId);
		findCategoryOrThrowNotFound(categoryId);

		UserCategory userCategoryTarget = getUserCategory(userId, categoryId);
		userCategoryTarget.setGoalAmount(target.getGoalAmount());
		return userCategoryRepo.save(userCategoryTarget);
	}

	@Override
	public int deleteCategoriesForUser(Long userId) {
		List<UserCategory> userCategories = userCategoryRepo.findByUser(findUserOrThrowNotFound(userId));
		userCategories.forEach(uc -> userCategoryRepo.delete(uc));
		return userCategories.size();
	}

	private UserCategory getUserCategory(Long userId, Long categoryId) {
		UserCategoryKey userCategoryKey = new UserCategoryKey(userId, categoryId);
		return userCategoryRepo.findById(userCategoryKey).get();
	}

	private Stream<UserCategory> getUserCategoryStream(User user) {
		Spliterator<UserCategory> allUsers = userCategoryRepo.findByUser(user).spliterator();
		return StreamSupport.stream(allUsers, false);
	}

	private Stream<com.vg.incubator.fft.usertransaction.Transaction> getTransactionStreamBetweenDateRange(Long userId,
			LocalDate startDate, LocalDate endDate) {
		Spliterator<com.vg.incubator.fft.usertransaction.Transaction> allCategories = transactionRepo
				.findByUserIdAndTransactionDateBetween(userId, startDate, endDate).spliterator();

		return StreamSupport.stream(allCategories, false);
	}

	@Override
	public List<com.vg.incubator.fft.usertransaction.Transaction> getTransactionStreamBetweenDateRangeForCategory(
			Long userId, Long categoryId, LocalDate startDate, LocalDate endDate) {
		Spliterator<com.vg.incubator.fft.usertransaction.Transaction> allCategories = transactionRepo
				.findByUserIdAndCategoryIdAndTransactionDateBetween(userId, categoryId, startDate, endDate)
				.spliterator();

		return StreamSupport.stream(allCategories, false).collect(Collectors.toList());
	}

	private User findUserOrThrowNotFound(Long id) {
		return userRepo.findById(id).orElseThrow(USER_NOT_FOUND);
	}

	private com.vg.incubator.fft.category.Category findCategoryOrThrowNotFound(Long id) {
		return categoryRepo.findById(id).orElseThrow(CATEGORY_NOT_FOUND);
	}

}

