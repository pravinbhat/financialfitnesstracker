package com.vg.incubator.fft.usercategory;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.Data;

public @Data class Target {
	@NotNull
	private BigDecimal goalAmount;
}
