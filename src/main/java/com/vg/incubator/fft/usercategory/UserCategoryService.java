package com.vg.incubator.fft.usercategory;

import java.time.LocalDate;
import java.util.List;

import com.vg.incubator.fft.user.User;
import com.vg.incubator.fft.usertransaction.Transaction;

public interface UserCategoryService {

	List<Category> getAllCategoriesWithTransactionInfoForUser(Long userId);

	List<UserCategory> createCategoriesForUser(User user);

	UserCategory updateUserCategory(Long userId, Long categoryId, Target target);

	int deleteCategoriesForUser(Long userId);

	Category getUserCategoryWithTransactionInfo(Long userId, Long categoryId);
	
	List<Transaction> getTransactionStreamBetweenDateRangeForCategory(Long userId, Long categoryId,
			LocalDate startDate, LocalDate endDate);

}
