package com.vg.incubator.fft.usercategory;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
public @Data @AllArgsConstructor @NoArgsConstructor class UserCategoryKey implements Serializable {

	private static final long serialVersionUID = -2667291359181652775L;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "category_id")
	private Long categoryId;
}
