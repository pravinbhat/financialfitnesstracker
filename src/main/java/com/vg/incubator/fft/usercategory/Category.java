package com.vg.incubator.fft.usercategory;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public @Data @AllArgsConstructor @NoArgsConstructor class Category {

	@NotNull
	private Long id;

	@NotNull
	private String categoryName;

	@NotNull
	private BigDecimal goalAmount;

	@NotNull
	private BigDecimal currentAmountSpent;

	@NotNull
	private BigDecimal lastMonthAmountSpent;
}
