package com.vg.incubator.fft.config;

import static com.vg.incubator.fft.util.FFTUtility.toLocalDateViaInstant;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vg.incubator.fft.category.Category;
import com.vg.incubator.fft.category.CategoryRepo;
import com.vg.incubator.fft.user.User;
import com.vg.incubator.fft.user.UserRepo;
import com.vg.incubator.fft.usertransaction.TransactionRepo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Component
public class SeedTransactionsLoader implements ApplicationRunner {

	private UserRepo userRepo;
	private CategoryRepo categoryRepo;
	private TransactionRepo txRepo;

	@Inject
	public SeedTransactionsLoader(UserRepo userRepo, CategoryRepo categoryRepo, TransactionRepo txRepo) {
		this.userRepo = userRepo;
		this.categoryRepo = categoryRepo;
		this.txRepo = txRepo;
	}

	@Override
	public void run(ApplicationArguments arg0) throws Exception {
		getTxFromJson().forEach(this::saveTransaction);
	}

	private List<Transaction> getTxFromJson() throws IOException, JsonParseException, JsonMappingException {
		TypeReference<List<Transaction>> typeReference = new TypeReference<List<Transaction>>() {
		};
		List<Transaction> transactions = new ObjectMapper()
				.readValue(new ClassPathResource("transactions.json").getInputStream(), typeReference);
		return transactions;
	}

	private void saveTransaction(Transaction transaction) {
		User user = userRepo.findByUsername(transaction.getUsername()).get(0);
		Category category = categoryRepo.findByName(transaction.getCategoryName()).get(0);
		txRepo.save(createTxObject(transaction, user, category));
	}

	private com.vg.incubator.fft.usertransaction.Transaction createTxObject(Transaction transaction, User user,
			Category category) {
		com.vg.incubator.fft.usertransaction.Transaction tx = new com.vg.incubator.fft.usertransaction.Transaction(
				user.getId(), category.getId(), transaction.getAmount(), transaction.getMerchant(),
				toLocalDateViaInstant(transaction.getTransactionDate()));
		return tx;
	}

}

@Data
@NoArgsConstructor
class Transaction {

	@NotNull
	private BigDecimal amount;

	@NotNull
	private Date transactionDate;

	@NotNull
	private String categoryName, merchant, username;
}
