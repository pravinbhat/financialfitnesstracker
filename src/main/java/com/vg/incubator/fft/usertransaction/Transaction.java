package com.vg.incubator.fft.usertransaction;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@SequenceGenerator(name = "Tx_SEQ", sequenceName = "tx_sequence")
public @Data @Entity @NoArgsConstructor class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Tx_SEQ")
	private Long id;

	@NotNull
	private Long userId;

	@NotNull
	private Long categoryId;

	@NotNull
	private BigDecimal amount;

	@NotNull
	private String merchant;

	@NotNull
	private LocalDate transactionDate;

	public Transaction(Long userId, Long categoryId, @NotNull BigDecimal amount, @NotNull String merchant,
			@NotNull LocalDate transactionDate) {
		this.userId = userId;
		this.categoryId = categoryId;
		this.amount = amount;
		this.merchant = merchant;
		this.transactionDate = transactionDate;
	}

}
