package com.vg.incubator.fft.usertransaction;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TransactionRepo extends CrudRepository<Transaction, Long> {
	List<Transaction> findByUserIdAndTransactionDateBetween(Long userId, LocalDate startDate, LocalDate endDate);

	List<Transaction> findByUserIdAndCategoryIdAndTransactionDateBetween(Long userId, Long categoryId,
			LocalDate startDate, LocalDate endDate);

}
