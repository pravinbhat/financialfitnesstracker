package com.vg.incubator.fft.user;

import static com.vg.incubator.fft.util.FFTUtility.checkDateForValidity;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.vg.incubator.fft.usercategory.UserCategory;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@SequenceGenerator(name = "User_SEQ", sequenceName = "user_sequence")
public @Getter @Entity class User {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "User_SEQ")
	private Long id;

	public User(String username, Date dateOfBirth) {
		this.username = username;
		this.dateOfBirth = dateOfBirth;
	}

	@NotNull
	@Setter
	private String username;

	@NotNull
	private Date dateOfBirth;

	@Setter
	private boolean hasConsented;

	@Setter
	private boolean hasGoals;

	@Setter
	@OneToMany(mappedBy = "user")
	private Set<UserCategory> userCategories = Collections.emptySet();

	public void setDate(String dateOfBirth) {
		this.dateOfBirth = checkDateForValidity(dateOfBirth);
	}
}
