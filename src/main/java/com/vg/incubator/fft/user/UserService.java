package com.vg.incubator.fft.user;

import java.util.List;

public interface UserService {

	List<UserModel> getAllUsers();

	UserModel getUser(Long id);
	
	List<String> getUserAlerts(Long id);

	UserModel createUser(UserModel userModel);

	UserModel updateUser(Long id, UserModel userModel);

}
