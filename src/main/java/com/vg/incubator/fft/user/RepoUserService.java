package com.vg.incubator.fft.user;

import static com.vg.incubator.fft.util.FFTUtility.USER_NOT_FOUND;
import static com.vg.incubator.fft.util.FFTUtility.checkDateForValidity;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Spliterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Named;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.vg.incubator.fft.usercategory.Category;
import com.vg.incubator.fft.usercategory.UserCategory;
import com.vg.incubator.fft.usercategory.UserCategoryService;

@Named
public class RepoUserService implements UserService {

	private UserRepo userRepo;
	private UserCategoryService userCategoryService;

	@Inject
	public RepoUserService(UserRepo userRepo, UserCategoryService userCategoryService) {
		this.userRepo = userRepo;
		this.userCategoryService = userCategoryService;
	}

	@Override
	public List<UserModel> getAllUsers() {
		return getUserStream().map(user -> {
			List<Category> categories = userCategoryService.getAllCategoriesWithTransactionInfoForUser(user.getId());
			return UserModel.fromUser(user, categories);
		}).collect(toList());
	}

	private Stream<User> getUserStream() {
		Spliterator<User> allUsers = userRepo.findAll().spliterator();
		return StreamSupport.stream(allUsers, false);
	}

	@Override
	public UserModel getUser(Long id) {
		List<Category> categories = userCategoryService.getAllCategoriesWithTransactionInfoForUser(id);

		return UserModel.fromUser(findUserOrThrowNotFound(id), categories);
	}

	@Override
	@HystrixCommand(fallbackMethod = "getUserAlertsFallback")
	public List<String> getUserAlerts(Long id) {
		throw new RuntimeException("No Alerts Service Found!");
	}
	
	@SuppressWarnings("unused")
	private List<String> getUserAlertsFallback(Long id) {
		return Arrays.asList("Alerts endpoint not yet implemented", "Check again later!");
	}

	@Override
	public UserModel createUser(UserModel userModel) {
		checkDateForValidity(userModel.getDateOfBirth());
		User user = userRepo.save(UserModel.toUser(userModel));
		List<Category> categories = userCategoryService.getAllCategoriesWithTransactionInfoForUser(user.getId());

		return UserModel.fromUser(user, categories);
	}

	@Override
	public UserModel updateUser(Long id, UserModel userModel) {
		User user = findUserOrThrowNotFound(id);
		user.setHasConsented(userModel.isHasConsented());
		Set<UserCategory> updatedCategories = updateCategories(userModel.isHasGoals(), user);
		user.setHasGoals(userModel.isHasGoals());
		user.setUserCategories(updatedCategories);
		List<Category> categories = userCategoryService.getAllCategoriesWithTransactionInfoForUser(user.getId());

		return UserModel.fromUser(userRepo.save(user), categories);
	}

	private User findUserOrThrowNotFound(Long id) {
		return userRepo.findById(id).orElseThrow(USER_NOT_FOUND);
	}

	private Set<UserCategory> updateCategories(boolean hasGoals, User user) {
		if (user.isHasGoals() == hasGoals) {
			return user.getUserCategories();
		}

		if (hasGoals) {
			return new HashSet<>(userCategoryService.createCategoriesForUser(user));
		}

		userCategoryService.deleteCategoriesForUser(user.getId());
		return Collections.emptySet();
	}

}
