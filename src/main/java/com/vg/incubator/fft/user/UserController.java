package com.vg.incubator.fft.user;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

	private static final String ERROR_MESSAGE_HEADER = "errormessage";
	private UserService userService;

	@Inject
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<List<UserModel>> getAllUsers() {
		return ResponseEntity.ok().body(userService.getAllUsers());
	}

	@GetMapping("/{userId}")
	public ResponseEntity<UserModel> getUser(@PathVariable Long userId) {
		try {
			return ResponseEntity.ok().body(userService.getUser(userId));
		} catch (UserNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		}
	}
	
	@GetMapping("/{userId}/alerts")
	public ResponseEntity<List<String>> getUserAlerts(@PathVariable Long userId) {
			return ResponseEntity.ok().body(userService.getUserAlerts(userId));
	}
	
	@PostMapping
	public ResponseEntity<UserModel> createUser(@RequestBody UserModel userModel) {
		if (StringUtils.isEmpty(userModel.getUsername()) || StringUtils.isEmpty(userModel.getUsername().trim())) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, "Username cannot be empty").build();
		}

		if (StringUtils.isEmpty(userModel.getDateOfBirth()) || StringUtils.isEmpty(userModel.getDateOfBirth().trim())) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, "Date of birth cannot be empty").build();
		}

		return ResponseEntity.ok().body(userService.createUser(userModel));
	}

	@CrossOrigin(origins = "*")
	@PutMapping("/{userId}")
	public ResponseEntity<UserModel> updateUser(@PathVariable Long userId, @RequestBody UserModel userModel) {
		try {
			return ResponseEntity.ok().body(userService.updateUser(userId, userModel));
		} catch (UserNotFoundException e) {
			return ResponseEntity.badRequest().header(ERROR_MESSAGE_HEADER, e.getMessage()).build();
		}
	}

}
