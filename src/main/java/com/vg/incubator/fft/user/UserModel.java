package com.vg.incubator.fft.user;

import static com.vg.incubator.fft.util.FFTUtility.checkDateForValidity;

import java.util.Collections;
import java.util.List;

import com.vg.incubator.fft.usercategory.Category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {
	private Long id;
	private String username;
	private String dateOfBirth;
	private boolean hasConsented;
	private boolean hasGoals;
	private List<Category> categories = Collections.emptyList();

	public static UserModel fromUser(User user, List<Category> categories) {
		return new UserModel(user.getId(), user.getUsername(), user.getDateOfBirth().toString(), user.isHasConsented(),
				user.isHasGoals(), categories);
	}

	public static User toUser(UserModel userModel) {
		User user = new User(userModel.getUsername(), checkDateForValidity(userModel.getDateOfBirth()));
		user.setHasConsented(userModel.isHasConsented());
		return user;
	}

}
