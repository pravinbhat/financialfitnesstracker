package com.vg.incubator.fft.user;

public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3212945046458213181L;

	public UserNotFoundException(String message) {
		super(message);
	}

}