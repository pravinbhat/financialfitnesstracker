package com.vg.incubator.fft.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.function.Supplier;

import com.vg.incubator.fft.category.CategoryNotFoundException;
import com.vg.incubator.fft.user.UserNotFoundException;

public class FFTUtility {

	public static final Supplier<UserNotFoundException> USER_NOT_FOUND = () -> new UserNotFoundException(
			"User not found!");
	public static final Supplier<CategoryNotFoundException> CATEGORY_NOT_FOUND = () -> new CategoryNotFoundException(
			"Category not found!");

	public static Date checkDateForValidity(String dateOfBirth) {
		try {
			return new SimpleDateFormat("dd-MM-yyyy").parse(dateOfBirth);
		} catch (ParseException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public static LocalDate toLocalDateViaInstant(Date dateToConvert) {
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	public static DateRange getDateRangeOfNow() {
		return DateRange.of(LocalDate.now());
	}

}
