package com.vg.incubator.fft.util;

import java.time.LocalDate;

import lombok.Data;

@Data
public class DateRange {

	private LocalDate begginingOfCurrentMonth, endOfCurrentMonth;
	private LocalDate begginingOfLastMonth, endOfLastMonth;

	public DateRange(LocalDate begginingOfCurrentMonth, LocalDate endOfCurrentMonth, LocalDate begginingOfLastMonth,
			LocalDate endOfLastMonth) {
		this.begginingOfCurrentMonth = begginingOfCurrentMonth;
		this.endOfCurrentMonth = endOfCurrentMonth;
		this.begginingOfLastMonth = begginingOfLastMonth;
		this.endOfLastMonth = endOfLastMonth;
	}

	static DateRange of(LocalDate of) {
		LocalDate begginingOfCurrentMonth = of.withDayOfMonth(1);
		LocalDate endOfCurrentMonth = begginingOfCurrentMonth.withDayOfMonth(begginingOfCurrentMonth.lengthOfMonth());
		return new DateRange(begginingOfCurrentMonth, endOfCurrentMonth, begginingOfCurrentMonth.minusMonths(1),
				endOfCurrentMonth.minusMonths(1));
	}

}