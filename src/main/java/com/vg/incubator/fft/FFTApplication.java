package com.vg.incubator.fft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

@SpringBootApplication
@EnableCircuitBreaker
public class FFTApplication {

	public static void main(String[] args) {
		SpringApplication.run(FFTApplication.class, args);
	}

}
