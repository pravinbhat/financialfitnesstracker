# FinancialFitnessTracker
Track your financial fitness!!

## Application folder 
./financialfitnesstracker

## To run the Application 
java -jar target/financialfitnesstracker-0.0.1.jar --spring.config.location=classpath:/application.properties

## This application was initialized with spring CLI using below command
spring init --dependencies=web,data-jpa financialfitnesstracker

## Access FFT H2 DB at 
http://localhost:8080/h2-console

spring.datasource.url=jdbc:h2:mem:fftdb
